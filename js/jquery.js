;( function( $, window, document, undefined ) {

    // Overwrite defaults call
    $.LiveValidator = LiveValidator.Plugin;

    // Plugin call on inputs
    $.fn.LiveValidator = function( options ) {
        return LiveValidator.Plugin.call( this, options, function( elems ) {
            return [].slice.call( elems );
        } );
    };
} )( jQuery, window, document );
