# LiveValidator - Plugin jQuery

[![build status](https://gitlab.com/LiveValidator/Plugin-jQuery/badges/master/build.svg)](https://gitlab.com/LiveValidator/Plugin-jQuery/commits/master)
[![coverage report](https://gitlab.com/LiveValidator/Plugin-jQuery/badges/master/coverage.svg)](https://gitlab.com/LiveValidator/Plugin-jQuery/commits/master)

Simple code that registers LiveValidator as a jQuery plugin, thereby making it callable via jQuery.

Find the project [home page and docs](https://chesedo.gitlab.io/LiveValidator/).
