/**
 * The test suite for the jquery plugin creation code (js/jquery.js)
 */

describe( 'Plugin jQuery', function() {
    it( 'defaults can be overwritten by calling `$.LiveValidator`', function() {
        $.LiveValidator( {
            requiredHTML: '*',
            themeData: {
                parentSelector: 'group'
            }
        } );

        expect( LiveValidator.defaults.requiredHTML ).toEqual( '*' );
        expect( LiveValidator.defaults.themeData.parentSelector ).toEqual( 'group' );
    } );

    it( 'allows only the first instantiation', function() {
        setFixtures( '<input />' );
        LiveValidator.defaults.theme = function fillerTheme() {};
        LiveValidator.defaults.theme.prototype.markRequired = function() {};
        LiveValidator.defaults.theme.prototype.unmarkRequired = function() {};
        LiveValidator.defaults.theme.prototype.setMissing = function() {};
        LiveValidator.defaults.theme.prototype.unsetMissing = function() {};
        LiveValidator.defaults.theme.prototype.clearErrors = function() {};
        LiveValidator.defaults.theme.prototype.addErrors = function() {};
        var input = $( 'input' )[ 0 ];

        $( input ).LiveValidator();
        $( input ).LiveValidator( { required: true } );
        expect( input.LiveValidator.options.required ).toEqual( false );
    } );
} );
